.SUFFIXES:
.SUFFIXES: .o .c
.PHONY: all clean install uninstall options

PREFIX = /usr/local
#CFLAGS = -g -O0 -Wall -Wextra -std=c89 -pedantic -D_POSIX_C_SOURCE=200809L
CFLAGS = -Os -Wall -Wextra -std=c89 -pedantic -D_POSIX_C_SOURCE=200809L
LDFLAGS = -ljack -lncurses -lm

BIN = jack_patchbay lbdemo
OBJ = ui.o util.o

all: options ${BIN}

options:
	@echo PREFIX = $(PREFIX)
	@echo CC = $(CC)
	@echo CFLAGS = $(CFLAGS)
	@echo LDFLAGS = $(LDFLAGS)

lbdemo: lbdemo.o
jack_patchbay: jack_patchbay.o
$(BIN): $(OBJ)
	$(CC) -o $@ $@.o $(OBJ) $(LDFLAGS)

.c.o:
	@echo CC $<
	@$(CC) $(CFLAGS) -c $<

clean:
	rm -rf *.o $(BIN)

install: all
	mkdir -p ${DESTDIR}${PREFIX}/bin
	cp -f jack_patchbay ${DESTDIR}${PREFIX}/bin
	chmod 755 ${DESTDIR}${PREFIX}/bin/jack_patchbay

uninstall:
	rm -f ${DESTDIR}${PREFIX}/bin/jack_patchbay
