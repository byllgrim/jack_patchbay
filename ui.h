enum
{
    TIMEOUTMS = 1000,
    MAXSTRLEN = 512
};

struct line
{
    char *str;
    int select;
    int mark;
    struct line *next;
};

struct listbox
{
    WINDOW *win;
    struct line *lns;
    size_t top;
    int select;
    int focus;
};

WINDOW *initcurses(void);
WINDOW *mkwin(WINDOW *, size_t, size_t, size_t, size_t);
struct listbox *newlb(WINDOW *);
void appendlb(struct listbox *, const char *);
void mvlbselect(struct listbox *, int);
void rmlbselect(struct listbox *);
void drawlb(struct listbox *);
void freelb(struct listbox *);
int getllsel(struct line *);
struct line *getlbline(struct line *, size_t);
struct line *findlbline(struct line *, const char *);
void updatelbdata(struct listbox *, const char **);
