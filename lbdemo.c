#include <curses.h>
#include <stdlib.h>

#include "ui.h"
#include "util.h"

enum
{
    WINWD = 40,
    WINHT = 20,
    WINXOFF = 10,
    WINYOFF = 3
};

static void
populatelb(struct listbox *lb)
{
    int i;
    char str[2 * WINWD];

    for (i = 0; i < WINHT + 4; i++)
    {
        snprintf(str,
                 sizeof(str) - 1,
                 "%03d long message that hopefully will overflow the lilwin",
                 i);
        appendlb(lb, str);
    }

    lb->lns->next->next->mark = 1;
    lb->lns->next->next->next->mark = 1;
}

int
main(void)
{
    WINDOW *termwin;
    WINDOW *lilwin;
    struct listbox *lb;
    int ch;

    initsignals();
    termwin = initcurses();
    lilwin = mkwin(termwin, WINHT, WINWD, WINYOFF, WINXOFF);
    lb = newlb(lilwin);
    populatelb(lb);

    for (ch = 0; ch != 'q'; ch = getch())
    {
        if (ch == 'j' && lb->select)
        {
            mvlbselect(lb, +1);
        }
        else if (ch == 'k' && lb->select)
        {
            mvlbselect(lb, -1);
        }
        else if (ch == 'x' && lb->select)
        {
            rmlbselect(lb);
        }
        else if (ch == 'a')
        {
            appendlb(lb, "appending");
        }
        else if (ch == 's')
        {
            lb->select = !lb->select;
        }
        else if (ch == 'c')
        {
            lb->focus = !lb->focus;
        }

        drawlb(lb);
        doupdate();
    }

    freelb(lb);
    return EXIT_SUCCESS;
}
