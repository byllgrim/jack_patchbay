# jack_patchbay

Terminal (ncurses) patchbay for jack audio connection kit, written in C89, used
to connect ports of jack clients.

# Usage

![screenshot](screenshot.png)

Terminal splits in two panes:
* Left: Output ports
* Right: Input ports

Keys:
* Navigate: __hjkl__ or __arrowkeys__, also __tab__
* Quit: __q__ or __^C__
* Connection: __c__ or __enter__

# Build

```
$ make
$ sudo make install
```

Dependencies:
* Jack1 or jack2
* ncurses (possibly just curses)
* Posix, i suppose
