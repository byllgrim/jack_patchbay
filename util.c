#include <curses.h>
#include <jack/jack.h>
#include <signal.h>
#include <stdlib.h>
#include <string.h>

#include "ui.h"
#include "util.h"

static void
sighnd(int signum)
{
    if (signum != SIGINT)
    {
        die("signal handler mismatch\n");
    }

    exit(EXIT_SUCCESS);
}

void
initsignals(void)
{
    struct sigaction sa;

    memset(&sa, 0, sizeof(sa));
    sa.sa_handler = sighnd;
    sigemptyset(&sa.sa_mask);
    sigaction(SIGINT, &sa, (void *)0);
}

void
die(char *msg)
{
    endwin();
    fprintf(stderr, msg);
    exit(EXIT_FAILURE);
}

void *
ecalloc(size_t nmemb, size_t size)
{
    void *ptr;

    ptr = calloc(nmemb, size);
    if (!ptr)
    {
        die("ecalloc failed\n");
    }

    return ptr;
}

int
isinlist(const char *str, const char **ls)
{
    if (!ls)
    {
        return 0;
    }

    for (; *ls; ls++)
    {
        if (!strncmp(str, *ls, jack_port_name_size()))
        {
            return 1;
        }
    }

    return 0;
}
