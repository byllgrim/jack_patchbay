#include <jack/jack.h>
#include <math.h>
#include <ncurses.h>
#include <stdlib.h>
#include <string.h>

#include "ui.h"
#include "util.h"

struct context
{
    jack_client_t *jc;
    struct listbox *llb;
    struct listbox *rlb;
    WINDOW *mainwin;
};

static void
createwindows(struct context *ctx)
{
    size_t ht;
    size_t lwd;
    size_t rwd;

    ht = LINES;
    lwd = COLS / 2;
    rwd = ceil(COLS / 2.0) - 1;
    ctx->llb->win = mkwin(ctx->mainwin, ht, lwd, 0, 0);
    ctx->rlb->win = mkwin(ctx->mainwin, ht, rwd, 0, COLS / 2 + 1);

    if (!ctx->llb->win || !ctx->rlb->win)
    {
        die("failed to create windows\n");
    }
}

static struct listbox *
getactivelb(struct context *ctx)
{
    if (ctx->llb->select)
    {
        return ctx->llb;
    }
    else if (ctx->rlb->select)
    {
        return ctx->rlb;
    }
    else
    {
        die("there is no active listbox\n");
    }

    return (void *)0;
}

static const char *
getcurport(struct context *ctx)
{
    struct listbox *lb;
    size_t curno;
    struct line *ln;

    lb = getactivelb(ctx);
    if (!lb)
    {
        die("found no active listbox\n");
    }

    curno = getllsel(lb->lns);
    ln = getlbline(lb->lns, curno);
    if (!ln)
    {
        die("no line at current active number\n");
    }

    return ln->str;
}

static void
lightconnected(struct context *ctx)
{
    const char **cons;
    const char *curname;
    jack_port_t *curport;
    struct listbox *rcv;
    struct line *ln;

    curname = getcurport(ctx);
    if (!curname)
    {
        die("selected a non-existing port!\n");
    }
    curport = jack_port_by_name(ctx->jc, curname);
    if (!curport)
    {
        die("no port by that name exist\n");
    }

    cons = jack_port_get_all_connections(ctx->jc, curport);
    rcv = (!ctx->llb->select) ? ctx->llb : ctx->rlb;
    for (ln = rcv->lns; ln; ln = ln->next)
    {
        ln->mark = !!isinlist(ln->str, cons);
    }
    jack_free(cons);
}

static void
drawui(struct context *ctx)
{
    lightconnected(ctx);
    drawlb(ctx->llb);
    drawlb(ctx->rlb);
    doupdate();
}

static void
initjack(struct context *ctx, const char *name)
{
    ctx->jc = jack_client_open(name, JackNoStartServer, (void*)0);
    if (!ctx->jc)
    {
        die("failed to open jack client\n");
    }
}

static void
polljack(struct context *ctx)
{
    const char **ins;
    const char **outs;

    ins = jack_get_ports(ctx->jc, (void*)0, (void*)0, JackPortIsInput);
    outs = jack_get_ports(ctx->jc, (void*)0, (void*)0, JackPortIsOutput);
    if (!ins || !outs)
    {
        die("failed to get jack ports\n");
    }

    updatelbdata(ctx->rlb, ins);
    updatelbdata(ctx->llb, outs);

    jack_free(ins);
    jack_free(outs);
}

static void
cleanup(struct context *ctx)
{
    endwin();
    freelb(ctx->llb);
    freelb(ctx->rlb);
    jack_client_close(ctx->jc);
}

static void
swapselected(struct context *ctx)
{
    ctx->llb->select = !ctx->llb->select;
    ctx->rlb->select = !ctx->rlb->select;
}

static int
isbothfocus(struct context *ctx)
{
    return (ctx->llb->focus && ctx->rlb->focus);
}

static void
clearfocuses(struct context *ctx)
{
    ctx->llb->focus = 0;
    ctx->rlb->focus = 0;
}

static int
isconnected(jack_client_t *jc, const char *srcn, const char *dstn)
{
    jack_port_t *srcp;

    srcp = jack_port_by_name(jc, srcn);
    if (!srcp)
    {
        die("made-up port names are never connected\n");
    }

    return jack_port_connected_to(srcp, dstn);
}

static void
toggleconnection(struct context *ctx)
{
    int srcno;
    int dstno;
    struct line *srcln;
    struct line *dstln;

    srcno = getllsel(ctx->llb->lns);
    dstno = getllsel(ctx->rlb->lns);
    srcln = getlbline(ctx->llb->lns, srcno);
    dstln = getlbline(ctx->rlb->lns, dstno);
    if (!srcln || !srcln->str || !dstln || !dstln->str)
    {
        die("can't connect invalid ports\n");
    }

    if (!isconnected(ctx->jc, srcln->str, dstln->str))
    {
        jack_connect(ctx->jc, srcln->str, dstln->str);
    }
    else
    {
        jack_disconnect(ctx->jc, srcln->str, dstln->str);
    }
}

static void
handlekeyboard(struct context *ctx, int ch)
{
    struct listbox *lb;

    switch (ch)
    {
    case 'j':
    case KEY_DOWN:
        lb = getactivelb(ctx);
        mvlbselect(lb, +1);
        break;
    case 'k':
    case KEY_UP:
        lb = getactivelb(ctx);
        mvlbselect(lb, -1);
        break;
    case 'h':
    case 'l':
    case KEY_RIGHT:
    case KEY_LEFT:
    case '\t':
        swapselected(ctx);
        break;
    case 'c':
    case '\n':
        lb = getactivelb(ctx);
        lb->focus = 1;

        if (isbothfocus(ctx))
        {
            toggleconnection(ctx);
            clearfocuses(ctx);
        }

        swapselected(ctx);
        break;
    }
}

static void
createlistboxes(struct context *ctx)
{
    ctx->llb = newlb((void *)0);
    ctx->rlb = newlb((void *)0);

    ctx->llb->select = 1;
}

int
main(void)
{
    struct context ctx = {0};
    int ch;

    initsignals();
    initjack(&ctx, "jack_patchbay");
    ctx.mainwin = initcurses();
    createlistboxes(&ctx);
    createwindows(&ctx);

    for (ch = 0; ch != 'q'; ch = getch())
    {
        polljack(&ctx);
        handlekeyboard(&ctx, ch);
        drawui(&ctx);
    }

    cleanup(&ctx);
    exit(EXIT_SUCCESS);
}
