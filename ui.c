#include <curses.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>

#include "ui.h"
#include "util.h"

static void
endcurses(void)
{
    endwin();
}

WINDOW *
initcurses(void)
{
    WINDOW *mainwin;

    mainwin = initscr();
    atexit(endcurses);
    wtimeout(mainwin, TIMEOUTMS);
    noecho();
    curs_set(0);
    keypad(mainwin, TRUE);

    return mainwin;
}

WINDOW *
mkwin(WINDOW *orig, size_t ht, size_t wd, size_t yoff, size_t xoff)
{
    WINDOW *win;

    win = subwin(orig, ht, wd, yoff, xoff);
    if (!win)
    {
        die("can't create subwin\n");
    }

    return win;
}

struct listbox *
newlb(WINDOW *win)
{
    struct listbox *lb;

    lb = ecalloc(1, sizeof(*lb));
    lb->win = win;

    return lb;
}

void
appendlb(struct listbox *lb, const char *str)
{
    struct line *lns;

    if (!lb->lns)
    {
        lb->lns = ecalloc(1, sizeof(*lb->lns));
        lb->lns->str = strdup(str);
        lb->lns->select = 1;
        return;
    }

    lns = lb->lns;
    for (; lns->next; lns = lns->next)
    {
    }
    lns->next = ecalloc(1, sizeof(*lns->next));
    lns->next->str = strdup(str);
}

struct line *
getlbline(struct line *lns, size_t num)
{
    size_t i;

    for (i = 0; lns && i < num; i++)
    {
        lns = lns->next;
    }

    return lns;
}

static void
truncstr(char *src, char *dst, size_t winlen, size_t dstlen)
{
    size_t cpylen;
    int copied;

    cpylen = fmin(winlen, dstlen - 1);
    copied = snprintf(dst, cpylen, src);
    if (copied < 0)
    {
        die("failed to truncate string\n");
    }
    dst[cpylen - 1] = '\0';
}

static void
drawlines(WINDOW *win, struct listbox *lb, struct line *ln)
{
    int i;
    int maxy;
    int maxx;
    char str[MAXSTRLEN];

    getmaxyx(win, maxy, maxx);

    for (i = 0; i < maxy && ln; i++, ln = ln->next)
    {
        if (lb->focus && !ln->select)
        {
            continue;
        }

        truncstr(ln->str, str, maxx + 1, sizeof(str));
        mvwprintw(win, i, 0, str);

        if (ln->mark && !lb->select)
        {
            mvwchgat(win, i, 0, -1, A_BOLD, 0, (void *)0);
        }
        if (ln->select && (lb->select || lb->focus))
        {
            mvwchgat(win, i, 0, -1, A_REVERSE, 0, (void *)0);
        }
    }
}

int
getllsel(struct line *ln)
{
    size_t i;

    for (i = 0; ln; i++)
    {
        if (ln->select)
        {
            break;
        }
        ln = ln->next;
    }

    return i;
}

static void
showselectedlb(struct listbox *lb)
{
    size_t pos;
    int maxy;
    int maxx;

    pos = getllsel(lb->lns);
    if (lb->top > pos)
    {
        lb->top = pos;
    }

    getmaxyx(lb->win, maxy, maxx);
    (void)maxx;
    if (pos >= lb->top + maxy)
    {
        lb->top++;
    }
}

void
drawlb(struct listbox *lb)
{
    struct line *ln;
    WINDOW *win;

    if (!lb || !lb->win)
    {
        die("can't draw lb without window\n");
    }
    win = lb->win;

    showselectedlb(lb);
    ln = getlbline(lb->lns, lb->top);
    if (!ln && lb->top != 0)
    {
        die("erronous top line number\n");
    }

    werase(win);
    drawlines(win, lb, ln);
    wnoutrefresh(win);
}

void
mvlbselect(struct listbox *lb, int sh)
{
    struct line *new;
    struct line *old;
    size_t orig;

    orig = getllsel(lb->lns);
    old = getlbline(lb->lns, orig);
    new = getlbline(lb->lns, orig + sh);
    if (!old || !new)
    {
        return;
    }

    old->select = 0;
    new->select = 1;
}

static void
freeline(struct line *ln)
{
    if (ln)
    {
        if (ln->str)
        {
            free(ln->str);
        }

        free(ln);
    }
}

static void
rmlbline(struct listbox *lb, int idx)
{
    struct line *ln;
    struct line *rmed;

    if (idx < 0)
    {
        die("removing illegal index\n");
    }

    if (idx == 0)
    {
        rmed = lb->lns;
        lb->lns = lb->lns->next;
    }
    else
    {
        ln = getlbline(lb->lns, idx - 1);
        rmed = ln->next;
        ln->next = ln->next->next;
    }

    freeline(rmed);
}

static void
yieldsel(struct listbox *lb, int idx)
{
    struct line *ln;

    if (idx == 0)
    {
        if (lb->lns->next)
        {
            lb->lns->next->select = 1;
        }
    }
    else
    {
        ln = getlbline(lb->lns, idx - 1);

        if (ln->next->next)
        {
            ln->next->next->select = 1;
        }
        else
        {
            ln->select = 1;
        }
    }
}

void
rmlbselect(struct listbox *lb)
{
    size_t num;

    if (!lb->lns)
    {
        return;
    }

    num = getllsel(lb->lns);
    yieldsel(lb, num);
    rmlbline(lb, num);
}

void
freelb(struct listbox *lb)
{
    struct line *ln;
    struct line *nxt;

    if (!lb)
    {
        return;
    }

    ln = lb->lns;
    for (; ln; ln = nxt)
    {
        nxt = ln->next;
        freeline(ln);
    }

    free(lb);
}

struct line *
findlbline(struct line *ln, const char *str)
{
    if (!ln)
    {
        return (void *)0;
    }

    for (; ln; ln = ln->next)
    {
        if (!ln->str)
        {
            return (void *)0;
        }

        if (strncmp(str, ln->str, MAXSTRLEN) == 0)
        {
            return ln;
        }
    }

    return (void *)0;
}

static void
addnodup(struct listbox *lb, const char *str)
{
    if (!findlbline(lb->lns, str))
    {
        appendlb(lb, str);
    }
}

void
updatelbdata(struct listbox *lb, const char **dat)
{
    struct line *ln;
    struct line *next;
    const char **tmp;
    int idx;

    for (tmp = dat; *tmp; tmp++)
    {
        addnodup(lb, *tmp);
    }

    idx = 0;
    for (ln = lb->lns; ln; ln = next)
    {
        next = ln->next;

        if (isinlist(ln->str, dat))
        {
            idx++;
        }
        else
        {
            if (ln->select)
            {
                yieldsel(lb, idx);
            }
            rmlbline(lb, idx);
        }
    }
}
